'use strict';

describe('Controller: PagingCtrl', function () {

  // load the controller's module
  beforeEach(module('spmbtestApp'));

  var PagingCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PagingCtrl = $controller('PagingCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PagingCtrl.awesomeThings.length).toBe(3);
  });
});
