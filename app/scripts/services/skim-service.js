'use strict';

/**
 * @ngdoc service
 * @name spmbtestApp.skimService
 * @description
 * # skimService
 * Factory in the spmbtestApp.
 */
angular.module('spmbtestApp')
  .factory('skimService', function ($http) {
   
    // Public API here
    return {
      getSkimList: function () {
        return $http.get('./api/training/skim-api.php');
      }
    };
  });
