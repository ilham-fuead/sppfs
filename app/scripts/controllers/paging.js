'use strict';

/**
 * @ngdoc function
 * @name spmbtestApp.controller:PagingCtrl
 * @description
 * # PagingCtrl
 * Controller of the spmbtestApp
 */
angular.module('spmbtestApp')
  .controller('PagingCtrl', ['$log', '$q', 'maklumatPenggunaService', function ($log, $q, PenggunaService) {

    var $ctrl = this;

    $ctrl.senaraiPengguna = [];
    $ctrl.Pengguna = {};

    $ctrl.isLoading = true;

    /*define default Paging Info Object */
    $ctrl.pagingInfoObj = {};
    $ctrl.currentPage = 1;
    var defaultPagingInfoObj = { pagingType: 2, totalRow: 0, totalPage: 1 };
    
    /* get All Pengguna Method */
    var queryPenggunaList = function (paramObj) {
      PenggunaService.getPagingInfo(paramObj).then(function (resp) {
        $ctrl.pagingInfoObj = resp.data;
        $ctrl.currentPage = 1;

        if ($ctrl.pagingInfoObj.totalRow === 0) {
          return $q.reject('No record to fetch');
        } else {
          return PenggunaService.getPagingData(paramObj);
        }

      }).then(function (resp) {
        if (angular.isArray(resp.data)) {
          $ctrl.senaraiPengguna = resp.data;
          $ctrl.isLoading = false;
        } else {
          return $q.reject('Data returned not an array');
        }

      }, function (err) {
        $ctrl.pagingInfoObj = defaultPagingInfoObj;
        $ctrl.senaraiPengguna = [];
        $ctrl.isLoading = false;
        $log.error('General Error : ', err);
      });
    };

    /*Method Page Number */
    $ctrl.setPage = function (pageNo) {
      $ctrl.currentPage = pageNo;
    };

    /*Method Change Page Number */
    $ctrl.pageChanged = function () {
      $ctrl.isLoading = true;
      $ctrl.Pengguna.pageNo = $ctrl.currentPage;

      PenggunaService.getPagingData($ctrl.Pengguna).then(function (resp) {
        $ctrl.senaraiPengguna = resp.data;
        $ctrl.isLoading = false;
      }, function (err) {
        $log.log('Error in firstTimeLoad : api ');
      });
      $log.log('Page changed to: ' + $ctrl.currentPage);
    };

    /* First Time load */
    $ctrl.firstTimeLoad = function () {
      $ctrl.isLoading = true;
      queryPenggunaList();
    };

    /*Call Method */
    $ctrl.firstTimeLoad();
  }]);
