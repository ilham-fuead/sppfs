<?php
class paging extends UniversalPaging{
    protected function handleTaskOnNoPaging() {
        echo '[]';
    }

    protected function performTaskOnEachPage(\DBQuery $DBQueryObj, $startRowIndex, $lastRowIndex) {
        while($row=$DBQueryObj->fetchRow()){
            $rows[]=$row;
        }

        $jsonRows=json_encode($rows,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);

        if(!$jsonRows){
            echo json_last_error_msg();
            exit();
        }

        echo $jsonRows;
    }

}
