'use strict';

describe('Service: maklumatPenggunaService', function () {

  // load the service's module
  beforeEach(module('spmbtestApp'));

  // instantiate service
  var maklumatPenggunaService;
  beforeEach(inject(function (_maklumatPenggunaService_) {
    maklumatPenggunaService = _maklumatPenggunaService_;
  }));

  it('should do something', function () {
    expect(!!maklumatPenggunaService).toBe(true);
  });

});
