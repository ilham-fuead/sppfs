<?php
require_once '../../vendor/autoload.php';
require_once '../config/db_connection.php';
include './paging.php';

const PAGING_SIZE=10;

$pageNo=1;

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

if (count($_GET)>0) {
    
    $pagingObj = (object) $_GET;   
    unset($_GET);

    /* Assign pageNo if supplied  */
    if(isset($pagingObj->pageNo)){
        $pageNo=$pagingObj->pageNo;
    }
}

$sql=<<<SQL
SELECT 
  nama,emel
FROM
  pengguna    
SQL;

//echo $sql;exit;

$myPagingObj=new paging($DBQueryObj);
$myPagingObj->setSQLStatement($sql);
$myPagingObj->setPagingProperty(IPagingType::MANUAL, PAGING_SIZE);
$myPagingObj->setPageProperty($myPagingObj->getPagingInfo());

$myPagingObj->startPaging($pageNo);

