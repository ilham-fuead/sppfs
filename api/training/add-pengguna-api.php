<?php
require_once '../../vendor/autoload.php';
require_once '../config/db_connection.php';

$inputObj=new MagicInput();
$inputObj->copy_RAW_JSON_properties();

//echo $inputObj->getJsonString();

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

$DBCmd=new DBCommand($DBQueryObj);

$DBCmd->setINSERTintoTable('pengguna');
$DBCmd->addINSERTcolumn('nama', $inputObj->nama, IFieldType::STRING_TYPE);
$DBCmd->addINSERTcolumn('emel', $inputObj->emel, IFieldType::STRING_TYPE);

$DBCmd->executeQueryCommand();

$statusObj=new MagicObject();

if($DBCmd->getExecutionStatus()==true){
    $statusObj->status=1;
}else{
    $statusObj->status=-1;
    $statusObj->error=$DBCmd->getError();
    $statusObj->sql=$DBCmd->getSQLstring();
}

echo $statusObj->getJsonString();

