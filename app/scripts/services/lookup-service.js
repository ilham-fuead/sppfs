'use strict';

/**
 * @ngdoc service
 * @name spmbtestApp.lookupService
 * @description
 * # lookupService
 * Factory in the spmbtestApp.
 */
angular.module('spmbtestApp')
  .factory('lookupService', ['$http',function ($http) {
    return {
      getSkimList: function () {
        return $http.get('./api/training/skim-api.php');
      }
    };
  }]);
