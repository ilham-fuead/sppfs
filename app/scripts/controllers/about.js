'use strict';

/**
 * @ngdoc function
 * @name spmbtestApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the spmbtestApp
 */
angular.module('spmbtestApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
