<?php
require_once '../../vendor/autoload.php';
require_once '../config/db_connection.php';
include './paging.php';

const PAGING_SIZE=10;

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

if (count($_GET)>0) {
    
    $conditionStr='';
    
    $pagingObj = (object) $_GET;   
    unset($_GET);

    /* Assign pageNo if supplied  */
    if(isset($pagingObj->pageNo)){
        $pageNo=$pagingObj->pageNo;
    }
}

$sql=<<<SQL
SELECT 
  nama,emel
FROM
  pengguna    
SQL;

$myPagingObj=new paging($DBQueryObj);
$myPagingObj->setSQLStatement($sql);
$myPagingObj->setPagingProperty(IPagingType::MANUAL, PAGING_SIZE);

echo json_encode($myPagingObj->getPagingInfo());