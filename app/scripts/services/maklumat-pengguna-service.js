'use strict';

/**
 * @ngdoc service
 * @name spmbtestApp.maklumatPenggunaService
 * @description
 * # maklumatPenggunaService
 * Factory in the spmbtestApp.
 */
angular.module('spmbtestApp')
  .factory('maklumatPenggunaService', ['$http', '$log', function ($http, $log) {

    // Public API here
    return {
      daftarPengguna: function (paramObj) {
        return $http.post('./api/training/add-pengguna-api.php', paramObj);
      },
      senaraiPengguna: function () {
        return $http.get('./api/training/pengguna-api.php');
      },
      getPagingInfo: function (paramsObj) {
        var config = {
          method: 'GET',
          url: './api/paging/get-maklumbalas-info.php',
          params: paramsObj
        };
        return $http(config);
      },
      getPagingData: function (paramsObj) {
        var config = {
          method: 'GET',
          url: './api/paging/get-maklumbalas-data.php',
          params: paramsObj
        };
        return $http(config);
      }
    };
  }]);
