'use strict';

/**
 * @ngdoc function
 * @name spmbtestApp.controller:ModalInstanceCtrl
 * @description
 * # ModalInstanceCtrl
 * Controller of the spmbtestApp
 */
angular.module('spmbtestApp')
  .controller('ModalInstanceCtrl', ['$uibModalInstance','items', function ($uibModalInstance,items) {

    var $ctrl = this;
    $ctrl.items = items;

    $ctrl.selected = {
      item: $ctrl.items[0]
    };

    $ctrl.ok = function () {
      $uibModalInstance.close($ctrl.selected.item);
    };

    $ctrl.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };


  }]);
