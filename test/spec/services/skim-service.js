'use strict';

describe('Service: skimService', function () {

  // load the service's module
  beforeEach(module('spmbtestApp'));

  // instantiate service
  var skimService;
  beforeEach(inject(function (_skimService_) {
    skimService = _skimService_;
  }));

  it('should do something', function () {
    expect(!!skimService).toBe(true);
  });

});
