'use strict';

/**
 * @ngdoc function
 * @name spmbtestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the spmbtestApp
 */
angular.module('spmbtestApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
